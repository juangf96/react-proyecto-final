import { useParams } from "react-router";
import axios from 'axios';
import { useEffect, useState } from "react";


let arrayAllegiances=[];
export function CharactersDetailPage(){
    const[character,setCharacters]= useState([])

    const[houses,setHouses]= useState([])

    const { name }= useParams();

    const getCharacter = ()=>{
        axios('https://api.got.show/api/show/characters/'+ name).then(res=>{
            console.log(name)
            console.log(res.data)
            setCharacters(res.data)
            console.log(character.allegiances)
            arrayAllegiances = character.allegiances
         });
    }

    const getHouses = ()=>{
        axios('https://api.got.show/api/show/houses/' + character.house).then(res=>{
            
        if(res.data[0] != undefined){
            setHouses(res.data[0])
            console.log(character.house)
            console.log(res.data[0])
        }
        });
    }
    // const pollita = getCharacter;
    // console.log(res)
    useEffect(()=>getCharacter(),[])

    useEffect(()=>getHouses(),[character.house])

    return(<div className="row">
                <div>
                    <h2>Name</h2>
                    {character.name}
                </div>
                <div>
                    <h2>Name</h2>
                    <img src={character.image} style={{width:300}}/>
                </div>
                <div>
                    <h2>Casa</h2>
                    <img src={houses.logoURL} style={{width:300}}/>
                </div>
     
            <div>
                <h2>Allegiances</h2>
                {Array.isArray(character.allegiances) ? character.allegiances.map( (item,i)=>
                <p key={i}>{item}</p>) : <p>{character.allegiances}</p> }
            </div>
            <div>
                <h2>Appearances</h2>
                {Array.isArray(character.appearances) ? character.appearances.map( (item,i)=>
                <p key={i}>{item}</p>) : <p>{character.appearances}</p> }
            </div>
            <div>
                <h2>Padre</h2>
                {character.father}
            </div>
            <div>
                <h2>Siblings</h2>
                {Array.isArray(character.siblings) ? character.siblings.map( (item,i)=>
                <p key={i}>{item}</p>) : <p>{character.siblings}</p> }
            </div>
            <div>
                <h2>Tittles</h2>
                {Array.isArray(character.titles) ? character.titles.map( (item,i)=>
                <p key={i}>{item}</p>) : <p>{character.titles}</p> }
            </div>
            




        </div>



    )
}