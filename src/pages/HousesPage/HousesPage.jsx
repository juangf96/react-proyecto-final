import { useState,useEffect } from "react"
import { GalleryHouses } from "./pages/Gallery/GalleryHouses";
import axios from 'axios';
import { SearchForm } from "../../shared/SearchForm/SearchForm";


let allHouses = [];
export function HousesPage(){
    const[houses,setHouses]= useState([])

    const getHouses = ()=>{
        axios('https://api.got.show/api/show/houses/').then(res=>{
            allHouses = res.data
            console.log(res.data)
            setHouses(res.data)
         });
    }

    const filterHouses = (filterHousesName)=>{
        const housesFiltered= allHouses.filter(houses =>
            houses.name.toLowerCase().includes(filterHousesName.toLowerCase()));
        setHouses(housesFiltered);
    }

    useEffect(getHouses,[])
    return(
        <div>
            <h1>ESTAMOS EN HOUSES PAGE</h1>
            <SearchForm fnFilter={filterHouses}/>
            <GalleryHouses housesList={houses}/>
        </div>
    )
}