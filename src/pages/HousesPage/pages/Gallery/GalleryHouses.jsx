import { Link } from 'react-router-dom';
import './Gallery.scss'
export function GalleryHouses(props){
    const houses = props.housesList;
    return(
        <div className="">
            <div className="row ">
            {houses.map((item,i)=>
                <div className="col-12 col-md-sm6 col-md-4 col-lg-3" style={{width:"300px"}} key={i}>
                <Link to={"/houses/"+ item.name}>
                {item.logoURL ? <img src={item.logoURL} alt =""/> : 
                <img src="https://spoilertime.com/wp-content/uploads/2017/12/House-Stark-Main-Shield.png" alt =""/> }
                    <div >{item.name}</div></Link>
                
                </div>
            )}
            </div>   
        </div>
)
}
