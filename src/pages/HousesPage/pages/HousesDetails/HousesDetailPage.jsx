import { useParams } from "react-router";
import axios from 'axios';
import { useEffect, useState } from "react";

export function HousesDetailPage(){
    const[houses,setHouses]= useState([])

    
    const { namehouse }= useParams();

    const getHouses = ()=>{
        axios('https://api.got.show/api/show/houses/'+ namehouse).then(res=>{
            console.log(namehouse)
            console.log(res.data)
            setHouses(res.data[0])
            
         });
    }

    
    useEffect(()=>getHouses(),[])

   

    return(
    <div className="row">
        <div>
            {houses.name}
        </div>
        <div>
        {houses.logoURL ? <img src={houses.logoURL} style={{width:300}}/> : 
                <img src="https://spoilertime.com/wp-content/uploads/2017/12/House-Stark-Main-Shield.png" style={{width:300}}/> }
        </div>
        <div>
            {houses.words}
        </div>
        <div>
            {houses.seat}
        </div>
        <div>
            {houses.region}
        </div>
        <div>
            <h2>Alianzas</h2>
            {Array.isArray(houses.allegiance) ? houses.allegiance.map( (item,i)=>
            <p key={i}>{item}</p>) : <p>{houses.allegiance}</p> }
        </div>
        <div>
            <h2>Relligiones</h2>
            {Array.isArray(houses.religion) ? houses.religion.map( (item,i)=>
            <p key={i}>{item}</p>) : <p>{houses.religion}</p> }
        </div>
        <div>
            <h2>Fundacion</h2>
            {houses.createdAt}
        </div>
        
        
    </div>

    )
}