import './Menu.scss';
import logo from '../../assets/image/home.svg';
import logos from '../../assets/image/flagen.svg';
import logs from '../../assets/image/flages.svg';
import { NavLink } from "react-router-dom";



export function Menu(){
    return(
        <div className="c-menu">
            <div className="two">
                <NavLink className="c-nav__link" exact to="/"><img className="c-menu__img" src={logo} alt=""/></NavLink>
                
                <img className="c-menu__img" src={logos} alt=""/>
                <img className="c-menu__img" src={logs} alt=""/>
            </div>
        </div>
    )

}