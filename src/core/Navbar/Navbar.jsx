import { NavLink } from "react-router-dom";
import './Navbar.scss';
export function Navbar(){
    return(
        <nav className="c-nav">
            {/* <NavLink className="c-nav__link" exact to="/">home</NavLink> */}
            <NavLink className="c-nav__link" to="/characters">Personajes</NavLink>
            <NavLink className="c-nav__link" to="/houses">Casas</NavLink>
            <NavLink className="c-nav__link" to="/chronology">Cronologia</NavLink>
        </nav>
    )

}