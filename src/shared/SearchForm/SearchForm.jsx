import { useForm } from "react-hook-form";


export function SearchForm(props){
    const { register,handleSubmit } = useForm();

    const filter = data =>{
        props.fnFilter(data.name);
     }
    return(
           <form>
               <label>
                   name
                   <input type="text" name="name" ref={register} onInput={handleSubmit(filter)}/>
               </label>
           </form>
    )
}